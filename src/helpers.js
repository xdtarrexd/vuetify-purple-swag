import { serialize } from 'object-to-formdata'

const httpBuildQuery = (obj) => {
  return Object.keys(obj)
    .map((k) => encodeURI(k) + '=' + encodeURI(obj[k]))
    .join('&')
}

// THis is useful when you need to upload files to an laravel app via axios
// all that is required is to replace the data sent with this
const laravelFormDataRequest = (
  $axios,
  method,
  url,
  obj,
  makeFilesSingleIfOnly1Instance = true
) => {
  const newData = {
    ...obj,
    _method: method,
  }
  const keys = Object.keys(newData)
  keys.forEach((key) => {
    let val = newData[key]
    if (
      makeFilesSingleIfOnly1Instance &&
      Array.isArray(val) &&
      val.length > 0 &&
      val[0] instanceof File
    ) {
      val = val[0]
    }
    newData[key] = val
  })
  const serializedData = serialize(newData)
  return $axios.post(url, serializedData)
}

const moneyFormatMixin = {
  methods: {
    moneyFormat(value) {
      return new Intl.NumberFormat('sv-SE', {
        style: 'currency',
        currency: 'SEK',
      }).format(value)
    },
  },
}

const tableFormatsArr = [
  'editable_money',
  'editable_text',
  'editable_textarea',
  'editable_date',
  'editable_datetime',
  'money',
  'yes_or_no',
]

export {
  httpBuildQuery,
  laravelFormDataRequest,
  moneyFormatMixin,
  tableFormatsArr,
}
