import { resolve } from 'path'

export default function swagModule(options) {
  const { nuxt } = this
  // Hook ready fn
  nuxt.hook('ready', (nuxt) => {
    // add plugin
    this.addPlugin({
      src: resolve(__dirname, 'plugins/inject.client.js'),
      options,
    })
  })
}
