const svSE = {
  swag_yes: 'Ja',
  swag_no: 'Nej',
  swag_select: 'Välj',
  swag_timeKick: 'Klockslag',
  swag_date: 'Datum',
  swag_paginationNext: 'Nästa &raquo;',
  swag_paginationPrev: '&laquo; Föregående',
  swag_paginationFirst: '&laquo; Första',
  swag_paginationLast: 'Sista &raquo;',
  swag_showMore: 'Visa mer',
  swag_cancel: 'Avbryt',
  swag_close: 'Stäng',
  swag_save: 'Spara',
  swag_delete: 'Ta bort',
  swag_update: 'Uppdatera',
  swag_search: 'Sök',
  swag_areYouSure: 'Är du säker?',
  swag_areYouSureBtnTrash: 'Är du säker på att du vill ta bort?',
  swag_areYouSureRmRow: 'Är du säker på att du vill ta bort raden?',
  swag_unknown: 'Okänd',
  swag_ok: 'Ok',
  swag_assignNewValue: 'Ange nytt värde',
  swag_noChangesMade: 'Inga ändringar gjorda',
}

const trans = (t) => {
  if (svSE[t]) {
    return svSE[t]
  } else {
    // eslint-disable-next-line no-console
    console.warn('Unknown trans ' + t)
    return t
  }
}

export default (context, inject) => {
  // inject placeholder translator if needed
  // eslint-disable-next-line no-undef
  if (!context.$t) {
    inject('t', trans)
  }
}
